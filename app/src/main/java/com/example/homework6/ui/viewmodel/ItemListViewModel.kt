package com.example.homework6.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.interaction.GetAllPhotosUseCase
import com.example.domain.interaction.RefreshPhotosUseCase
import com.example.domain.model.Photos
import kotlinx.coroutines.launch


class ItemListViewModel (
    private val getAllPhotosUseCase: GetAllPhotosUseCase,
    private val refreshPhotosUseCase: RefreshPhotosUseCase
) : ViewModel()  {

    //LiveData envuelve un objeto Lista de fotos para almacenar el resultado de consultar la BBDD
    lateinit var photos: LiveData<List<Photos>>

    private val _navigateToSelectedPhoto = MutableLiveData<Photos?>()

    var photoSelected : Photos? = null



    val navigateToSelectedPhoto: LiveData<Photos?>
        get() = _navigateToSelectedPhoto

    init {
        refreshData()
        getPhotosFromRepository()
    }

    private fun refreshData() {
        viewModelScope.launch {
            try {
                refreshPhotosUseCase.invoke()
            } catch (e: Exception) {
            }
        }
    }

    private fun getPhotosFromRepository() {
        viewModelScope.launch {
            try {
                photos = getAllPhotosUseCase.invoke()

            } catch (e: Exception) {
            }
        }
    }

    fun displayPhotoDetails(photo: Photos) {
        _navigateToSelectedPhoto.value = photo
        photoSelected = photo
    }

    fun displayPhotoDetailsComplete() {
        _navigateToSelectedPhoto.value = null
    }
 }
package com.example.homework6.ui.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.homework5beta1.databinding.FragmentItemlistBinding
import com.example.homework6.ui.viewmodel.ItemListViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class ItemListFragment : Fragment() {

    private val viewModel by sharedViewModel<ItemListViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val binding = FragmentItemlistBinding.inflate(inflater)

        binding.viewModel = viewModel

        binding.lifecycleOwner = this

        binding.recyclerViewPosts.adapter = ItemsPostAdapter(ItemsPostAdapter.OnClickListener {
            viewModel.displayPhotoDetails(it)
        })

        viewModel.navigateToSelectedPhoto.observe(viewLifecycleOwner, {
            if (it != null) {
                this.findNavController().navigate(
                    ItemListFragmentDirections.actionItemListFragmentToImageDetailFragment(it)
                )
                viewModel.displayPhotoDetailsComplete()
            }
        })

        return binding.root

    }
}
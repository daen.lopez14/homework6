package com.example.homework6.ui.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.homework5beta1.databinding.ImageDetailFragmentBinding
import com.example.homework6.ui.viewmodel.ItemListViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class ImageDetailFragment : Fragment() {

    private val viewModel by sharedViewModel <ItemListViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = ImageDetailFragmentBinding.inflate(inflater)

        binding.lifecycleOwner = this

        binding.viewModel = viewModel

        return binding.root
    }

}
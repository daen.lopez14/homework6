package com.example.homework6.ui.viewmodel

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.example.domain.model.Photos


class ImageDetailViewModel(savedStateHandle: SavedStateHandle) : ViewModel() {

    val selectedPhoto: Photos = savedStateHandle.get<Photos>(PHOTO_SELECTED)!!


    companion object {
        private const val PHOTO_SELECTED = "selectedPhoto"
    }
}

package com.example.homework6.utils

import com.example.homework6.ui.viewmodel.ImageDetailViewModel
import com.example.homework6.ui.viewmodel.ItemListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {

    viewModel { ItemListViewModel(get(), get()) }
    viewModel { params -> ImageDetailViewModel(params.get()) }
}
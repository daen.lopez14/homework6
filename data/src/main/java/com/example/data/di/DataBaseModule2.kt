package com.example.data.di

import androidx.room.Room
import com.example.data.database.PhotosDataBase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

private const val WEATHER_DB = "Photos-DDBB"

val databaseModule = module {
    single {
        //TODO remove fallbackToDestructiveMigration when this goes to production
        Room.databaseBuilder(androidContext(), PhotosDataBase::class.java, WEATHER_DB).build()
    }
    factory { get<PhotosDataBase>().photosDao }
}
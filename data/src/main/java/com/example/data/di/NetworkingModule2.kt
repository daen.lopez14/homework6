package com.example.data.di

import com.example.data.BuildConfig
import com.example.data.networking.PhotosApi
import java.util.concurrent.TimeUnit
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val BASE_URL: String =
    "https://jsonplaceholder.typicode.com"

val networkingModule = module {
    single { GsonConverterFactory.create() as Converter.Factory }
    single {
        OkHttpClient.Builder().apply {
            if (BuildConfig.DEBUG)
                addNetworkInterceptor(
                    HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
                ).callTimeout(10, TimeUnit.SECONDS)
        }.build()
    }
    single {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(get())
            .addConverterFactory(get())
            .build()
    }
    single { get<Retrofit>().create(PhotosApi::class.java) }
}
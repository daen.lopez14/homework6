package com.example.data.di

import com.example.data.repository.PhotoRepositoryImpl
import com.example.domain.repository.PhotoRepository
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val repositoryModule = module {
    factory<PhotoRepository> { PhotoRepositoryImpl(get(), get()) }
}
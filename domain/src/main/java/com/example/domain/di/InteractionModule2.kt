package com.example.domain.di

import com.example.domain.interaction.GetAllPhotosUseCase
import com.example.domain.interaction.GetAllPhotosUseCaseImpl
import com.example.domain.interaction.RefreshPhotosUseCase
import com.example.domain.interaction.RefreshPhotosUseCaseImpl
import org.koin.dsl.module

val interactionModule = module {
    factory<GetAllPhotosUseCase> { GetAllPhotosUseCaseImpl(get()) }
    factory<RefreshPhotosUseCase> { RefreshPhotosUseCaseImpl(get()) }
}